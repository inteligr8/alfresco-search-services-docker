
FROM ${source.docker.repo}:${source.docker.tag}

ADD docker-entrypoint.sh /usr/local/bin

USER root
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh
USER 33007

# Execute the Docker container initialization script
ENTRYPOINT [ "/usr/local/bin/docker-entrypoint.sh" ]

# Start Alfresco Search Services
CMD [ "/opt/alfresco-search-services/solr/bin/search_config_setup.sh", "/opt/alfresco-search-services/solr/bin/solr start -f" ]
