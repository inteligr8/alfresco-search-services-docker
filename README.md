
# Alfresco Search Service Docker Image Enhanced

This is a mirror of the Alfresco provided Docker image for Alfresco Search Service.  The only difference is the following features.

## Environment Variable Expansion

The following environment variables result in changes to the `shared.properties` and all template `solrcore.properties` files.

| Environment Variable                    | Description |
| --------------------------------------- | ----------- |
| `SOLR_ALFRESCO_HOST` or `ALFRESCO_HOST` | The ACS platform host to call for indexing and shard cluster heartbeat. |
| `SOLR_ALFRESCO_PORT` or `ALFRESCO_PORT` | The port of the ACS platform host above. |
| `ALFRESCO_CONTEXT`                      | The path context root of the ACS platform host above. |
| `SOLR_SOLR_HOST` or `SOLR_HOST`         | This application's host as addressible from the ACS platform for search calls and heartbeat checks. |
| `SOLR_SOLR_PORT` or `SOLR_PORT`         | The port of this application as addressible from the ACS platform. |
| `SHARD_METHOD`                          | The Solr sharding method for any new shards. |
| `SHARD_RANGE`                           | The Solr sharding range for method `DB_ID_RANGE`. |
| `SHARD_KEY`                             | The Solr sharding key for multiple methods. |
| `SHARD_DATE_GROUPING`                   | The Solr sharding date grouping for method `DATE`. |
| `SHARD_REGEX`                           | The Solr sharding regular expression for method `PROPERTY`. |