#!/bin/sh

# translating legacy envvars to new ones; allowing legacy to overwrite
if [[ ! -z "$SOLR_SOLR_HOST" ]]; then
    SOLR_HOST=$SOLR_SOLR_HOST
fi
if [[ ! -z "$SOLR_SOLR_PORT" ]]; then
    SOLR_PORT=$SOLR_SOLR_PORT
fi
if [[ ! -z "$SOLR_ALFRESCO_HOST" ]]; then
    ALFRESCO_HOST=$SOLR_ALFRESCO_HOST
fi
if [[ ! -z "$SOLR_ALFRESCO_PORT" ]]; then
    ALFRESCO_PORT=$SOLR_ALFRESCO_PORT
fi

# property files to possibly change
SHARED_PROPS=/opt/alfresco-search-services/solrhome/conf/shared.properties
CORE_PROPS_LIST="/opt/alfresco-search-services/solrhome/templates/rerank/conf/solrcore.properties /opt/alfresco-search-services/solrhome/templates/noRerank/conf/solrcore.properties"

if [[ ! -z "$SOLR_HOST" ]]; then
    sed -i 's/[# ]*solr\.host=.*/solr.host='$SOLR_HOST'/' $SHARED_PROPS
    sed -i 's/[# ]*solr\.port=.*/solr.port='$SOLR_PORT'/' $SHARED_PROPS
fi

for CORE_PROPS in $CORE_PROPS_LIST; do
    if [[ ! -z "$ALFRESCO_HOST" ]]; then
        sed -si 's/[# ]*alfresco\.host=.*/alfresco.host='$ALFRESCO_HOST'/' $CORE_PROPS

        if [[ "$ALFRESCO_SECURE_COMMS" == "https" ]]; then
            sed -si 's/[# ]*alfresco\.port.ssl=.*/alfresco.port.ssl='$ALFRESCO_PORT'/' $CORE_PROPS
            sed -si 's/[# ]*alfresco\.secureComms=.*/alfresco.secureComms=https/' $CORE_PROPS
        else
            sed -si 's/[# ]*alfresco\.port=.*/alfresco.port='$ALFRESCO_PORT'/' $CORE_PROPS
            sed -si 's/[# ]*alfresco\.secureComms=.*/alfresco.secureComms=none/' $CORE_PROPS
        fi

        if [[ ! -z "$ALFRESCO_CONTEXT" ]]; then
            sed -si 's~[# ]*alfresco\.baseUrl=.*~alfresco.baseUrl=/'$ALFRESCO_CONTEXT'~' $CORE_PROPS
        fi
    fi

    if [[ ! -z "$SHARD_METHOD" ]]; then
        sed -si 's/[# ]*shard\.method=.*/shard.method='$SHARD_METHOD'/' $CORE_PROPS
    fi
    if [[ ! -z "$SHARD_RANGE" ]]; then
        echo -e "\nshard.range=$SHARD_RANGE" >> $CORE_PROPS
    fi
    if [[ ! -z "$SHARD_KEY" ]]; then
        echo -e "\nshard.key=$SHARD_KEY" >> $CORE_PROPS
    fi
    if [[ ! -z "$SHARD_DATE_GROUPING" ]]; then
        echo -e "\nshard.date.grouping=$SHARD_DATE_GROUPING" >> $CORE_PROPS
    fi
    if [[ ! -z "$SHARD_REGEX" ]]; then
        echo -e "\nshard.regex=$SHARD_REGEX" >> $CORE_PROPS
    fi
done

exec "$@"
